package com.example.cleancode.service;

import com.example.cleancode.entity.Product;
import com.example.cleancode.repository.ProductRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Objects;
import java.util.Optional;


@Service
@Slf4j
public class ProductService {

    @Autowired
    private ProductRepository productRepository;

    public List<Product> getAllProducts() {
        return productRepository.findAll();
    }

    public String addProduct(Product product){
        try {
            //Check for request body empty or not
            if (Objects.nonNull(product)) {
                productRepository.save(product);
            }
        }catch (Exception e){
            log.error("Invalid Product Details");
        }
        return "Product Added Successfully";
    }

    public String removeProduct(Long productId){
        //Check for product exist with given id
        Optional<Product> productOptional = productRepository.findById(productId);
        if(productOptional.isEmpty()){
            log.error("Product not found for given product id : {}",productId);
            throw new RuntimeException();
        }
        productRepository.deleteById(productId);
        return "Product Deleted Successfully";
    }
}
