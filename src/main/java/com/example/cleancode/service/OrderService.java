package com.example.cleancode.service;

import com.example.cleancode.entity.Order;
import com.example.cleancode.entity.Product;
import com.example.cleancode.repository.OrderRepository;
import com.example.cleancode.repository.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

@Service
public class OrderService {
    @Autowired
    private OrderRepository orderRepository;

    @Autowired
    private ProductRepository productRepository;

    public List<Order> getAllOrders() {
        return orderRepository.findAll();
    }

    public Order placeOrder(List<Long> productIds) {
        List<Product> products = productRepository.findAllById(productIds);
        if (products.isEmpty()) {
            throw new IllegalArgumentException("No products found with the provided IDs.");
        }

        Order order = new Order();
        order.setProducts(products);
        order.setOrderDate(LocalDateTime.now());
        order.setCancelled(false);

        return orderRepository.save(order);
    }

    public void cancelOrder(Long orderId) {
        //Check Order is present for given order id
        Optional<Order> optionalOrder = orderRepository.findById(orderId);
        if (optionalOrder.isPresent()) {
            Order order = optionalOrder.get();
            if (!order.isCancelled()) {
                order.setCancelled(true);
                orderRepository.save(order);
            } else {
                throw new IllegalStateException("Order is already cancelled.");
            }
        } else {
            throw new IllegalArgumentException("No order found with the provided ID.");
        }
    }
}

