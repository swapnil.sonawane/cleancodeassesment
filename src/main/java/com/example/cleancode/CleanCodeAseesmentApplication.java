package com.example.cleancode;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CleanCodeAseesmentApplication {

	public static void main(String[] args) {
		SpringApplication.run(CleanCodeAseesmentApplication.class, args);
	}

}
