package com.example.cleancode.controller;

import com.example.cleancode.entity.Product;
import com.example.cleancode.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/product")
public class ProductController {

    @Autowired
    private ProductService productService;

    @GetMapping("/get-all")
    private List<Product> getAllProducts(){
        return productService.getAllProducts();
    }

    @PostMapping("/add")
    private String addProduct(@RequestBody Product product){
        return productService.addProduct(product);
    }

    @DeleteMapping("/{productId}")
    private String deleteProductById(@PathVariable(required = true) Long productId){
        return productService.removeProduct(productId);
    }
}
