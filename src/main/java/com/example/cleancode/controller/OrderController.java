package com.example.cleancode.controller;

import com.example.cleancode.entity.Order;
import com.example.cleancode.service.OrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/order")
public class OrderController {

    @Autowired
    private OrderService orderService;

    @GetMapping("/get-all")
    private List<Order> getAllOrders(){
        return orderService.getAllOrders();
    }

}
